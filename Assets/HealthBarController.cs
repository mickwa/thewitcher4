﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthBarController : MonoBehaviour
{
  public RectTransform fill;
  private float _startWidth;
  private SnakeQueenController _sqc;

  private void Start()
  {
    _startWidth = GetComponent<RectTransform>().rect.width;
    _sqc = FindObjectOfType<SnakeQueenController>();
    _sqc.OnQueenHealthChanged += _sqc_OnQueenHealthChanged;
    _sqc.OnQueenKilled += _sqc_OnQueenKilled;
  }

  private void _sqc_OnQueenKilled()
  {
    gameObject.SetActive(false);
  }

  private void _sqc_OnQueenHealthChanged()
  {
    UpdateHealthBarValue();
  }

  void UpdateHealthBarValue()
  {
    fill.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, _startWidth * _sqc.GetHealth_01());
  }
}
