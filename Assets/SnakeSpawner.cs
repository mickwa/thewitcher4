﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SnakeSpawner : MonoBehaviour
{
  [SerializeField]
  GameObject SnakePrefab;

  [Range(0, 1)]
  public float SnakeSpawnThreashold;

  void Start()
  {
    foreach (Transform t in transform)
    {
      if (Random.Range(0, 1f) < SnakeSpawnThreashold)
      {
        GameObject newSnake = Instantiate(SnakePrefab, t.position, Quaternion.identity);
        newSnake.name = t.name;
      }
    }
  }
}