﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotationTests : MonoBehaviour
{
 public bool mode;

  // Update is called once per frame
  void Update()
  {
    if (mode)
    {
      transform.RotateAround(transform.position, Vector3.up, 90f * Time.deltaTime);
      transform.Rotate(0, 90f * Time.deltaTime, 0);
    }
    else
    {
      transform.Rotate(0, 90f * Time.deltaTime, 0);
      transform.RotateAround(transform.position, Vector3.up, 90f * Time.deltaTime);
    }
  }
}
