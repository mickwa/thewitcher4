﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SnakeQueenController : MonoBehaviour
{
  public AudioClip roarSound;
  public AudioClip hurtSound;
  public GameObject HealthBar;

  AudioSource _audioSource;

  Animator anim;
  ParticleSystem[] emitters;

  void Start()
  {
    emitters = GetComponentsInChildren<ParticleSystem>();
    anim = GetComponent<Animator>();
    _audioSource = GetComponent<AudioSource>();
    StopAcidSpit();
    QueenAlive = true;
  }
  
  void Update()
  {
    return;
     if (Input.GetKeyDown(KeyCode.A))
      anim.SetTrigger("SnakeQueenAttack");

    if (Input.GetKeyDown(KeyCode.D))
    {
      anim.SetTrigger("SnakeQueenDead");
      QueenAlive = false;
      if (OnQueenKilled != null) OnQueenKilled();
    }
  }
  public void StartAttacking()
  {
    InvokeRepeating("Attack", 5, 6);
  }

  private void Attack()
  {
    anim.SetTrigger("SnakeQueenAttack");
  }
  public void StartAcidSpit()
  {
    Debug.Log("start");
    foreach (ParticleSystem p in emitters)
      p.Play();
  }

  public void StopAcidSpit()
  {
    foreach(ParticleSystem p in emitters)
    p.Stop();
  }

  public void Roar()
  {
    _audioSource.clip = roarSound;
    _audioSource.Play();
  }
  private int queenStartHealth = 10;
  private int queenHealth = 10;
  
  public bool QueenAlive{ get; private set; }

  public event System.Action  OnQueenKilled;
  public event System.Action OnQueenHealthChanged;

  public void OnCollisionEnter(Collision collision)
  {
    Debug.Log("Queen touched: "+collision.gameObject.name);
    if (QueenAlive)
    {
      if (collision.gameObject.name == "Sword")
      {
        _audioSource.clip = hurtSound;
        queenHealth--;
        _audioSource.Play();
        if (OnQueenHealthChanged != null) OnQueenHealthChanged();
      }
      if (queenHealth <= 0)
      {
        anim.SetTrigger("SnakeQueenDead");
        QueenAlive = false;
        if (OnQueenKilled != null) OnQueenKilled();
      }
    }
  }

  public float GetHealth_01()
  {
    return (float)queenHealth / (float)queenStartHealth;
  }

  private void OnBecameVisible()
  {
    HealthBar.SetActive(true);
  }


}
