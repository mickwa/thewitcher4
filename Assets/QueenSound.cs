﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QueenSound : MonoBehaviour
{
  public GameObject QueenHealthBar;

  AudioSource _audioSource;
  bool played = false;
  // Use this for initialization
  void Start()
  {
    _audioSource = GetComponent<AudioSource>();
  }

  private void OnTriggerEnter(Collider other)
  {
    if(played == false && other.name == "FPSController")
    {
      played = true;
      _audioSource.Play();
      FindObjectOfType<SnakeQueenController>().StartAttacking();
      QueenHealthBar.SetActive(true);    
    }
  }
}