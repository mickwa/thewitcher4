﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CaveEntrance : MonoBehaviour
{
  private void OnTriggerEnter(Collider other)
  {
    Debug.Log("Entered cave: "+other.name);
    if(other.name == "FPSController")
    {
      Light l = other.gameObject.GetComponentInChildren<Light>();
      l.intensity = 4;
      l.range = 20;
    }
  }

}