﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpitController : MonoBehaviour
{
  SwordController _swordController;

  // Use this for initialization
  void Start()
  {
    _swordController = FindObjectOfType<SwordController>();
  }

  private void OnParticleCollision(GameObject other)
  {
    Debug.Log(other.name);
    if (other.name == "FPSController")
    {
      _swordController.ChangeHealth(-1);
    }
  }
}