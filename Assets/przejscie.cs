﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class przejscie : MonoBehaviour {
  SnakeQueenController sqc;
  private void Start()
  {
    sqc = FindObjectOfType<SnakeQueenController>();
    sqc.OnQueenKilled += Sqc_OnQueenKilled;
  }

  private void Sqc_OnQueenKilled()
  {
    GetComponent<BoxCollider>().enabled = false;
  }
}
