﻿using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Characters.FirstPerson;

public class SwordController : MonoBehaviour
{
  public AudioClip[] swordSounds;
  public AudioClip hurtSound;
  AudioSource _audioSource;
  private float WiedzminMaxHealth = 100;
  private float WiedzminHealth = 100;
  public event Action OnHealthChange;
  public bool isAttacking { get; private set; }
  Animator anim;
  string ChangeSwordAnimationMethodName = "ChangeSwordAnimation";
  CharacterController fpc;
  // Use this for initialization
  void Start()
  {
    anim = GetComponent<Animator>();
    fpc = FindObjectOfType<CharacterController>();
    _audioSource = GetComponent<AudioSource>();
  }

  // Ta funkcja jest dodana jako event do animacji.
  public void PlaySwordSound(int i)
  {
    _audioSource.clip = swordSounds[UnityEngine.Random.Range(0,swordSounds.Length)];
    _audioSource.Play();
  }

  // Update is called once per frame
  void Update()
  {
    if (Input.GetKeyDown(KeyCode.Escape)) Application.Quit();

    if (TextChanger.przeczytane == false) return;
    anim.SetFloat("WalkSpeed", Mathf.InverseLerp(0, 5, fpc.velocity.magnitude));
    if(Input.GetMouseButtonDown(0))
    {
     // Debug.Log("Start Attack");
      isAttacking = true;
      anim.SetBool("IsAttacking", isAttacking);
      InvokeRepeating(ChangeSwordAnimationMethodName, 0.5f, 0.5f);
    }

    if (Input.GetMouseButtonUp(0))
    {
    //  Debug.Log("End Attack");
      isAttacking = false;
      anim.SetBool("IsAttacking", isAttacking);
      CancelInvoke(ChangeSwordAnimationMethodName);
    }
  }

  private void ChangeSwordAnimation()
  {
    float newValue = UnityEngine.Random.Range(0, 3) / 2f;
    //Debug.Log(newValue);
    anim.SetFloat("SwordAttackAnimationBlend", newValue);
  }

  public float GetHealth_01()
  {
    return WiedzminHealth / WiedzminMaxHealth;
  }

  public void ChangeHealth(int value)
  {
    if(value <0)
    {
      _audioSource.clip = hurtSound;
      _audioSource.Play();

    }
    WiedzminHealth += value;
    if (OnHealthChange != null) OnHealthChange();
  }

  private void OnTriggerEnter(Collider other)
  {
    Debug.Log(other.name);
  }


}
