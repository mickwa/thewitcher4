﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SnakeController : MonoBehaviour
{
  private Transform wiedzminFeet;
  public float lookPointYOffset;

  SphereCollider _sphereCollider;
  private void Start()
  {
    wiedzminFeet = GameObject.Find("WiedzminFeet").transform;
    _sphereCollider = GetComponentInChildren<SphereCollider>();
    InvokeRepeating("CheckShouldAttack", 0, 1f);
  }

  void CheckShouldAttack()
  {
    float distance = Vector3.Distance(transform.position, wiedzminFeet.position);
    bool shouldAttack = ((Random.Range(0, 4)) % 3 == 0);
    if (distance < 3 && shouldAttack)
    {
      GetComponent<Animator>().SetTrigger("Attack");
    }
  }

  void Update()
  {
    if (Input.GetKeyDown(KeyCode.Z))
    {
    //  GetComponent<Animator>().SetTrigger("Attack");
    }

    // tego teraz nie uzywam.
    //  GetComponent<Animator>().SetTrigger("SpitAcid");

    transform.LookAt(wiedzminFeet.position + Vector3.up * lookPointYOffset);
    transform.RotateAround(transform.position, Vector3.up, 90);

  //  Debug.DrawLine(transform.position, transform.GetChild(0).position, Color.red);
  //  Debug.DrawLine(transform.position, transform.GetChild(1).position, Color.red);
  }

  private void OnCollisionEnter(Collision collision)
  {
    Debug.Log("d");
  }

  // Ta funkcja jest dodana jako event do animacji.
  // zwieksza collider aby dziabnac wiedzmina albo zmniejsza go z powrotem
  public void SetColliderSize(float radius)
  {
    _sphereCollider.radius = radius;
  }
}