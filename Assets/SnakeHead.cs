﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class SnakeHead : MonoBehaviour
{
  SwordController sc;
  public GameObject HeadPrefab;
  public GameObject BodyPrefab;
  [SerializeField]
  new SkinnedMeshRenderer renderer;
  private bool _isAlive = true;


  void Start()
  {
    sc = FindObjectOfType<SwordController>();
  }
    
  void KillSnake(Vector3 position)
  {
    Debug.Log("Dealing damage to snake");
    Instantiate(HeadPrefab, position, transform.rotation);
    Instantiate(BodyPrefab, position, transform.rotation);
    GetComponent<AudioSource>().Play();
    renderer.enabled = false;
    _isAlive = false;
    Destroy(gameObject.transform.parent.gameObject,3f);
  }

  private void OnCollisionEnter(Collision collision)
  {
    if (collision.gameObject.name == "Sword" && sc.isAttacking && _isAlive)
    {
      KillSnake(collision.contacts[0].point);
    }

    if (collision.gameObject.name == "FPSController" && sc.isAttacking == false)
    {
      Debug.Log("Dealing damage to player");
      sc.ChangeHealth(-2);
    }
  }
 
}
