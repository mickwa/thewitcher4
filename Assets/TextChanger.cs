﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextChanger : MonoBehaviour
{
  public MeshRenderer miecz;
  public MeshRenderer reka;
  public GameObject about;

  string[] teksty = { "Dobrze, że już jesteś Wiedźminie. Czekałem na ciebie.\n(spacja - dalej) ",
                      "To w tej oto pieczarze mieszka owa kreatura która nęka naszą wioskę.",
 // "Pożera świnie i owce, ostatnio porwała moją starą pewnie myśląc, że to krowa.",
  "Ponoć poczwara ma dwie głowy plujące jadowitą plwociną!",
  "Ale nie bój nic panie Wiedźmin! Trzymaj tutaj tą Mithrilową zbroję którą zabraliśmy jakiemuś niziołkowi.",
   "Oraz ten oto miecz który świeci na niebiesko kiedy węże są w pobliżu.",
  // "Ponoć działa też z orkami ale ich tu na szczęście nie ma.",
  // "Mówił że spieszy się zniszczyć jakiś pierścień. Ale po co niszczyć pierścień skoro można go sprzedać w lombardzie?",
  // "Niziołka rzuciliśmy na pożarcie tej kreaturze żeby zostawiła warchlaki.",
   "Generalnie jako że jesteś Wiedźminem i masz Mithrilową zbroję to węże nic nie mogą ci zrobić.",
   "Tak więc Twoją rolą jest wparować do tej groty, zrobić zadymę i ubić ten diabelski pomiot który się tam szarogęsi.",
 //  "Poczwarę trzeba tłuc po czerepach aż do skutku.",
 //  "Będę czekał na Ciebie, aby dać Ci mieszek złota.",
      "Powodzenia!",
//=========
//"Dziękujemy Wiedźminie! Oto Twoja zapłata! \n(Daje mieszek złota)\nTo koniec przygody!",
//"Autor: mickwa",
//"kontakt: mickwa@op.pl",
//"www.mickwadev.com",
//"programowanie: ja",
//"grafika 3D: ja",
//"tekstury: ja",
//"dźwięki: freesound.org",

  };


  public static bool przeczytane = false;
  Text txt;
  int index = 0;
  SnakeQueenController sqc;
  // Use this for initialization
  void Start()
  {
    txt = GetComponent<Text>();
    txt.text = teksty[0];
    sqc = FindObjectOfType<SnakeQueenController>();
    sqc.OnQueenKilled += Sqc_OnQueenKilled;
  }

  private void Sqc_OnQueenKilled()
  {
    index = 0;
    transform.parent.gameObject.SetActive(true);
    txt.text = "Dziękujemy Wiedźminie! Oto Twoja zapłata! \n(Daje mieszek złota)\n";
    about.SetActive(true);
  }

  // Update is called once per frame
  void Update()
  {
    if (sqc.QueenAlive)
    {
      if (Input.GetKeyDown(KeyCode.Space)) index++;
      if (index < teksty.Length)
      {
        txt.text = teksty[index];
      }

      if (index == 3)
      {
        reka.enabled = true;
      }

      if (index == 4)
      {
        miecz.enabled = true;
      }

      if (index == teksty.Length)
      {
        transform.parent.gameObject.SetActive(false);
        przeczytane = true;
      }
    }
  }
}
